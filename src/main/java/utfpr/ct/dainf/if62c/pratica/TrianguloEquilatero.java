/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author lvale
 */
public class TrianguloEquilatero extends Retangulo{
    
    private double lado;
    
    public TrianguloEquilatero(double lado)
    {
        super(lado,lado);
        this.lado = lado;
    }
    
    @Override
    public String getNome()
    {
        return this.getClass().getSimpleName();
    }
    @Override
    public double getPerimetro()
    {
        return lado*3;
    }
    @Override
    public double getArea()
    {
        return lado*lado*Math.sqrt(3)/4;
    }
    @Override
    public double getLadoMenor()
    {
        return lado;
    }
    @Override
    public double getLadoMaior()
    {
        return lado;
    }

}
