package utfpr.ct.dainf.if62c.pratica;


public class Elipse implements FiguraComEixos{
    
    private double r;
    private double s;
    
    public Elipse(double r, double s)
    {
        this.r = r;
        this.s = s;
    }
    
    
    
    
    @Override
    public double getArea()
    {
        return Math.PI*r*s;
    }
    @Override
    public double getPerimetro()
    {
        return Math.PI*((3*(r+s)) - Math.sqrt((3*r + s)*(r+3*s))) ;     
    }    
    @Override
    public String getNome() 
    {
        return this.getClass().getSimpleName();
    }
    @Override
    public double getEixoMenor()
    {
        return s;
    }
    @Override
    public double getEixoMaior()
    {
        return r;
    }
}
