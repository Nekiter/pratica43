package utfpr.ct.dainf.if62c.pratica;

public class Circulo extends Elipse{
    
    public Circulo(double raio)
    {
        super(raio,raio);
    }
    
    
    @Override
    public double getPerimetro()
    {
        return 2*Math.PI*getEixoMaior();   
    }
}