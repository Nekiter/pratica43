package utfpr.ct.dainf.if62c.pratica;

public class Retangulo implements FiguraComLados{
    
    private double r;
    private double s;
    
    public Retangulo(double r, double s)
    {
        this.r = r;
        this.s = s;
    }
    
    @Override
    public String getNome()
    {
        return this.getClass().getSimpleName();
    }
    @Override
    public double getPerimetro()
    {
        return r*2+s*2;
    }
    @Override
    public double getArea()
    {
        return r*s;
    }
    @Override
    public double getLadoMenor()
    {
        return r;
    }
    @Override
    public double getLadoMaior()
    {
        return s;
    }
}
