
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lvale
 */
public class Pratica43 {
    public static void main(String args[])
    {
        //Criando objetos e instanciando os objetos
        Elipse elipse = new Elipse(1, 2);
        Circulo circulo = new Circulo(1);
        Quadrado quadrado = new Quadrado(1);
        Retangulo retangulo = new Retangulo(1, 2);
        TrianguloEquilatero triangulo = new TrianguloEquilatero(1);
        
        
        System.out.println("Area do Circulo");
        System.out.println(circulo.getArea());
        
        System.out.println("Perimetro do Circulo");
        System.out.println(circulo.getPerimetro());
        
        System.out.println("Area do elipse");
        System.out.println(elipse.getArea());
        
        System.out.println("Perimetro do elipse");
        System.out.println(elipse.getPerimetro());
        
        System.out.println("Area do Quadrado");
        System.out.println(quadrado.getArea());
        
        System.out.println("Perimetro do Quadrado");
        System.out.println(quadrado.getPerimetro());
        
        System.out.println("Area do Retangulo");
        System.out.println(retangulo.getArea());
        
        System.out.println("Perimetro do Retangulo");
        System.out.println(retangulo.getPerimetro());
        
        System.out.println("Area do Triangulo");
        System.out.println(triangulo.getArea());
        
        System.out.println("Perimetro do Triangulo");
        System.out.println(triangulo.getPerimetro());
        
        
    }
    
}
